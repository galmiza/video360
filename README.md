## About this project
The project "video360" is about developing a 360° field of view camera that can produce content for virtual reality headsets such as the Occulus Rift.

The content should be a representation of the whole surrounding environment. Unfortunately no single optic camera has a field of view close to 360°. Several cameras perfectly synchronized have to be used to capture the whole environment. Some image processing is also required to merge the camera streams into a standard flat video stream.

### The photosphere
An optimized and easy to use image format to represent the surrounding environment is the photosphere:

![Photosphere](http://photosphereviewer.net/images/demo/demo1_full.jpg)

### Ultra high resolution required!
In order to provide "enough" resolution to the user (who has a view field of roughtly 100°x100°), the resolution of the video (which covers 360°x180°) should be 4K or even 8K.

The current hardware components to achieve real time video encoding at such resolutions are not yet available or still very expensive thus not yet suitable for consumer products.

So the project simply demonstrates through a prototype how to get and merge in real time the video streams of several webcams to produce a 360°x180° videosphere stream.

### Environment representations (cubemap vs spheremap)

* Cube map wrapping: 6 cameras can represent the whole surrounding environment:

![Cube map wrapping](http://www.f-lohmueller.de/pov_tut/backgrnd/im/CubeMappingWrapping_1d_64.gif)


* Sphere map unwrapping: the environment can be unwrapped from its spherical representation to a plane

![Sphere map unwrapping](http://webserver2.tecgraf.puc-rio.br/~fmiranda/INF2608/t2/img/spheremapping.png)



## The prototype
Using a cheap Raspberry Pi and USB webcams, it is possible to produce in real time (~720p@30fps) a videosphere stream.

The software is quite simple and involves developments in C with the libraries OpenGL (to have hardware accelerated image processing) and OpenCV (to extract video stream from USB cameras).

## Repository content
This repository contains all the sources of the prototype, the compilation procedures and should let you build the prototype at home!

## Demo
A [web app](http://www.galmiza.net/tools/video360/builder/mapping.html) (using WebGL) lets you see in real time the transformation of 6 pictures into a photosphere!

* Move the mouse horizontally to change the field of view of the cameras (make it 90° to have the cameras cover the whole 360°x180°)
* Move the mouse vertically to rotate the camera around an arbitrary axis (this operation distorts the whole photosphere)