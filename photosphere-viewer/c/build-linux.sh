flags="-w -Os -s -static-libgcc -static-libstdc++"

g++ $flags -c -o main.o src/main.c -I src
g++ $flags -o main main.o -L lib -lglut -lGL -lGLEW -lGLU
rm main.o
./main
