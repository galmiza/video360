@echo off

set main=main
set srcdir=%CD%
set mingw=d:\MinGW\bin
set flags=-w -Os -s -static-libgcc -static-libstdc++

set source="%srcdir%\src\%main%.c"
set object="%srcdir%\%main%.o"
set output="%srcdir%\%main%.exe"
set include="%srcdir%\src"
set lib="%srcdir%\lib"

cd /d %mingw%

@echo on
g++ %flags% -c -o %object% %source% -I %include%
g++ %flags% -o %output% %object% -L %lib% -lfreeglut -lopengl32 -lglu32
del %object%

pause