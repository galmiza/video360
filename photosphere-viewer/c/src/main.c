#define M_PI 3.14159265359
#define M_PI_2 1.57079632679

#include <stdio.h>
#include <stdlib.h> // pulls in declaration of malloc, free
#include <vector>
#include <math.h>
#ifdef __APPLE__
 #include <GLUT/glut.h>
#else
 #include <GL/glut.h>
#endif
#include "SolidSphere.h"


using namespace std;


SolidSphere sphere(1, 120, 240);
float phi = 0;
float theta = 0;

void displayCall() {
  
  // Frame buffer dimensions
  int const win_width  = 800;
  int const win_height = 800;
  float const win_aspect = (float)win_width / (float)win_height;
  
  // Init viewport
  glViewport(0, 0, win_width, win_height);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  // Define projection
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  gluPerspective(90, win_aspect, 0.1, 10);
  
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
  gluLookAt(0.0, 0.0, 0.0, // from
            cos(theta)*sin(phi), sin(theta), cos(theta)*cos(phi), // to
            0.0, 1.0, 0.0);
		  
  //glPolygonMode(GL_FRONT, GL_LINE);
  //glPolygonMode(GL_BACK, GL_LINE);
  sphere.draw(0, 0, 0);
        
  glutSwapBuffers();
} /* end func displayCall */


/* Mouse events */
void keyPressed(int key, int x, int y) {
  switch(key) {
  case GLUT_KEY_UP:    	theta += 0.03f;  break;	
  case GLUT_KEY_DOWN:   theta -= 0.03f;  break;
  case GLUT_KEY_LEFT:   phi += 0.03f;    break;
  case GLUT_KEY_RIGHT:  phi -= 0.03f;    break;
  }
  glutPostRedisplay();
}

/* Load texture */
GLuint LoadTexture( const char * filename, int width, int height ) {

  GLuint texture;
  unsigned char * data;

  FILE * file;
  file = fopen( filename, "rb" );

  if ( file == NULL ) return 0;
  data = (unsigned char *)malloc( width * height * 3 );
  fread( data, width * height * 3, 1, file );
  fclose( file );

  // Swap r and b color channels
  for(int i = 0; i < width * height ; ++i) {
    int index = i*3;
    unsigned char R,B;
    B = data[index];
    R = data[index+2];

    data[index] = R;
    data[index+2] = B;
  }

  glGenTextures( 1, &texture );
  glBindTexture( GL_TEXTURE_2D, texture );
  glTexEnvf( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE,GL_MODULATE );
  //( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_NEAREST );
  //glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,GL_LINEAR );
  glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,GL_NEAREST );
  glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,GL_NEAREST );
  glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S,GL_REPEAT );
  glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T,GL_REPEAT );
  gluBuild2DMipmaps( GL_TEXTURE_2D, 3, width, height,GL_RGB, GL_UNSIGNED_BYTE, data );
  free( data );

  return texture;
} /* end func LoadTexture */


/* Set up everything, and start the GLUT main loop. */
int main(int argc, char *argv[]) {
  glutInit(&argc, argv);
  glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH);
  glutInitWindowSize(800, 800);
  glutInitWindowPosition(300, 200);
  glutCreateWindow("Photosphere viewer");
  glutDisplayFunc(displayCall);
  glutSpecialFunc(keyPressed);
  
  // Setup opengl params
  glDisable(GL_CULL_FACE);
  glDisable(GL_DEPTH_TEST);
  glEnable(GL_TEXTURE_2D);
  LoadTexture("res/canvas.bmp", 1024, 1024);
  
  glutMainLoop();
  return 0;
} /* end func main */
